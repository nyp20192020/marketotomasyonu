#ifndef MOANAPENCERE_H
#define MOANAPENCERE_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui {
class MOAnaPencere;
}
QT_END_NAMESPACE

class MOAnaPencere : public QMainWindow {
  Q_OBJECT

public:
  MOAnaPencere(QWidget *parent = nullptr);
  ~MOAnaPencere();

public slots:
  virtual void close();

private slots:
  void on_actionYeniMusteriEkle_triggered();

  void on_actionYeniUrunEkle_triggered();

  void on_actionYeniSiparis_triggered();

  void on_actionKaydet_triggered();

  void on_actionAc_triggered();

  void on_actionMusteriListesi_triggered();

  void on_actionNormal_triggered();

  void temaDegistir();

  void temaSeciminiTemizle();

  void on_actionUrunListesi_triggered();

  void on_actionM_teri_Listesi_triggered();

private:
  Ui::MOAnaPencere *ui;

  QString _qtStili;
  void applyStyle(const QString &temaAdi);
};
#endif // MOANAPENCERE_H
