#ifndef MOOURUNLISTESI_H
#define MOOURUNLISTESI_H

#include <QDialog>

#include <VeriYonetim/urunveriyoneticisi.h>

namespace Ui {
class MOOUrunListesi;
}

class MOOUrunListesi : public QDialog {
  Q_OBJECT

public:
  explicit MOOUrunListesi(QWidget *parent = nullptr);
  ~MOOUrunListesi();

private:
  void tabloGuncelle();

private slots:
  void fiyatOperatoruGuncelle();

  void filtrele();

private:
  Ui::MOOUrunListesi *ui;

  UrunVeriYoneticisi::Liste _urunler;
};

#endif // MOOURUNLISTESI_H
