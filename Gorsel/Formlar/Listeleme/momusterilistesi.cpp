#include "momusterilistesi.h"
#include "ui_momusterilistesi.h"

#include "Formlar/VeriGiris/moyenimusterigirisformu.h"

#include <veritabani.h>

#include <QIcon>
#include <QMessageBox>
#include <QPushButton>
#include <QStringList>
#include <QTableWidgetItem>

MOMusteriListesi::MOMusteriListesi(QWidget *parent)
    : QDialog(parent), ui(new Ui::MOMusteriListesi) {
  ui->setupUi(this);

  this->_musteriler =
      Veritabani::vt().musteri().ara([](Musteri::Ptr) { return true; });
  this->tabloGuncelle();
}

MOMusteriListesi::~MOMusteriListesi() { delete ui; }

void MOMusteriListesi::tabloGuncelle() {
  QStringList basliklar;
  basliklar << tr("Müşteri No") << tr("Adı") << tr("Soyadı")
            << tr("Doğum Tarihi") << tr("Adres") << tr("Kayıt Sil")
            << tr("Müşteri Düzenle");

  ui->tblMusteri->setColumnCount(7);
  ui->tblMusteri->setRowCount(this->_musteriler.count());

  ui->tblMusteri->setHorizontalHeaderLabels(basliklar);

  QIcon silmeSimgesi = QIcon();
  silmeSimgesi.addFile(QString::fromUtf8(":/resimler/Sil.png"), QSize(),
                       QIcon::Normal, QIcon::Off);

  QIcon duzenlemeSimgesi = QIcon();
  duzenlemeSimgesi.addFile(QString::fromUtf8(":/resimler/Guncelle.png"),
                           QSize(), QIcon::Normal, QIcon::Off);

  for (int i = 0; i < this->_musteriler.count(); i++) {
    auto hucre = new QTableWidgetItem();
    hucre->setText(tr("%1").arg(this->_musteriler[i]->id()));
    hucre->setTextAlignment(Qt::AlignCenter);
    ui->tblMusteri->setItem(i, 0, hucre);

    hucre = new QTableWidgetItem();
    hucre->setText(this->_musteriler[i]->adi());
    hucre->setTextAlignment(Qt::AlignCenter);
    ui->tblMusteri->setItem(i, 1, hucre);

    hucre = new QTableWidgetItem();
    hucre->setText(this->_musteriler[i]->soyadi());
    hucre->setTextAlignment(Qt::AlignCenter);
    ui->tblMusteri->setItem(i, 2, hucre);

    hucre = new QTableWidgetItem();
    hucre->setText(this->_musteriler[i]->dogumTarihi().toString());
    hucre->setTextAlignment(Qt::AlignCenter);
    ui->tblMusteri->setItem(i, 3, hucre);

    hucre = new QTableWidgetItem();
    hucre->setText(this->_musteriler[i]->adres());
    hucre->setTextAlignment(Qt::AlignCenter);
    ui->tblMusteri->setItem(i, 4, hucre);

    auto silmeButonu = new QPushButton(this);
    silmeButonu->setText(tr("Müşteri Sil"));
    silmeButonu->setIcon(silmeSimgesi);
    ui->tblMusteri->setCellWidget(i, 5, silmeButonu);
    auto musteri = this->_musteriler[i];
    connect(silmeButonu, &QPushButton::clicked, [musteri, this]() {
      auto cevap =
          QMessageBox::question(nullptr, tr("Silme Onayı"),
                                tr("%1 %2 silmek istediğinize emin misiniz?")
                                    .arg(musteri->adi())
                                    .arg(musteri->soyadi()));
      if (cevap == QMessageBox::Yes) {
        Veritabani::vt().musteri().sil(musteri);
        this->filtrele();
      }
    });

    auto duzenlemeButonu = new QPushButton(this);
    duzenlemeButonu->setText(tr("Müşteri Düzenle"));
    duzenlemeButonu->setIcon(duzenlemeSimgesi);
    ui->tblMusteri->setCellWidget(i, 6, duzenlemeButonu);

    connect(duzenlemeButonu, &QPushButton::clicked, [musteri, this]() {
      MOYeniMusteriGirisFormu form(this, musteri);
      form.setStyleSheet(this->styleSheet());
      form.exec();
      this->filtrele();
    });
  }
}

void MOMusteriListesi::filtrele() {
  MusteriVeriYoneticisi::FiltreFonksiyonu adFiltreFonksiyonu =
      [](Musteri::Ptr) { return true; };
  MusteriVeriYoneticisi::FiltreFonksiyonu soyadFiltreFonksiyonu =
      [](Musteri::Ptr) { return true; };
  MusteriVeriYoneticisi::FiltreFonksiyonu tarihFiltreFonksiyonu =
      [](Musteri::Ptr) { return true; };

  if (ui->chkAdi->isChecked()) {
    QString aranan = ui->leAdi->text();
    if (ui->cmbAdiOperator->currentIndex() == 0) {
      // İle Başlayanlar
      adFiltreFonksiyonu = [aranan](Musteri::Ptr eleman) {
        return eleman->adi().toLower().startsWith(aranan.toLower());
      };
    } else if (ui->cmbAdiOperator->currentIndex() == 1) {
      // İle Bitenler
      adFiltreFonksiyonu = [aranan](Musteri::Ptr eleman) {
        return eleman->adi().toLower().endsWith(aranan.toLower());
      };
    } else {
      // İçerenler
      adFiltreFonksiyonu = [aranan](Musteri::Ptr eleman) {
        return eleman->adi().toLower().contains(aranan.toLower());
      };
    }
  }

  if (ui->chkSoyadi->isChecked()) {
    QString aranan = ui->leSoyadi->text();
    if (ui->cmbSoyadiOperator->currentIndex() == 0) {
      // İle Başlayanlar
      soyadFiltreFonksiyonu = [aranan](Musteri::Ptr eleman) {
        return eleman->soyadi().toLower().startsWith(aranan.toLower());
      };
    } else if (ui->cmbSoyadiOperator->currentIndex() == 1) {
      // İle Bitenler
      soyadFiltreFonksiyonu = [aranan](Musteri::Ptr eleman) {
        return eleman->soyadi().toLower().endsWith(aranan.toLower());
      };
    } else {
      // İçerenler
      soyadFiltreFonksiyonu = [aranan](Musteri::Ptr eleman) {
        return eleman->soyadi().toLower().contains(aranan.toLower());
      };
    }
  }

  if (ui->chkDogumTarihi->isChecked()) {
    QDate baslangic = ui->deBaslangic->date();
    QDate bitis = ui->deBitis->date();
    tarihFiltreFonksiyonu = [baslangic, bitis](Musteri::Ptr eleman) {
      return eleman->dogumTarihi() >= baslangic &&
             eleman->dogumTarihi() <= bitis;
    };
  }

  auto filtreFonksiyonu = [adFiltreFonksiyonu, soyadFiltreFonksiyonu,
                           tarihFiltreFonksiyonu](Musteri::Ptr eleman) {
    return adFiltreFonksiyonu(eleman) && soyadFiltreFonksiyonu(eleman) &&
           tarihFiltreFonksiyonu(eleman);
  };

  this->_musteriler = Veritabani::vt().musteri().ara(filtreFonksiyonu);
  this->tabloGuncelle();
}

void MOMusteriListesi::on_btnAra_clicked() { this->filtrele(); }

void MOMusteriListesi::canliAra() {
  if (ui->chkCanliAra->isChecked()) {
    this->filtrele();
  }
}
