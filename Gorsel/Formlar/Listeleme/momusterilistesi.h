#ifndef MOMUSTERILISTESI_H
#define MOMUSTERILISTESI_H

#include <QDialog>

#include <VeriYonetim/musteriveriyoneticisi.h>

namespace Ui {
class MOMusteriListesi;
}

class MOMusteriListesi : public QDialog {
  Q_OBJECT

public:
  explicit MOMusteriListesi(QWidget *parent = nullptr);
  ~MOMusteriListesi();

private slots:
  void on_btnAra_clicked();

  void canliAra();

private:
  void tabloGuncelle();

  void filtrele();

private:
  Ui::MOMusteriListesi *ui;

  MusteriVeriYoneticisi::Liste _musteriler;
};

#endif // MOMUSTERILISTESI_H
