#include "moourunlistesi.h"
#include "ui_moourunlistesi.h"

#include <QMessageBox>
#include <QStringList>
#include <QTableWidgetItem>

#include <veritabani.h>

MOOUrunListesi::MOOUrunListesi(QWidget *parent)
    : QDialog(parent), ui(new Ui::MOOUrunListesi) {
  ui->setupUi(this);

  this->filtrele();
}

MOOUrunListesi::~MOOUrunListesi() { delete ui; }

void MOOUrunListesi::tabloGuncelle() {
  QStringList basliklar;
  basliklar << tr("Ürün No") << tr("Ürün Adı") << tr("Fiyatı")
            << tr("Kayıt Sil") << tr("Ürün Düzenle");

  ui->tblListe->setColumnCount(5);
  ui->tblListe->setRowCount(this->_urunler.count());

  ui->tblListe->setHorizontalHeaderLabels(basliklar);

  QIcon silmeSimgesi = QIcon();
  silmeSimgesi.addFile(QString::fromUtf8(":/resimler/Sil.png"), QSize(),
                       QIcon::Normal, QIcon::Off);

  QIcon duzenlemeSimgesi = QIcon();
  duzenlemeSimgesi.addFile(QString::fromUtf8(":/resimler/Guncelle.png"),
                           QSize(), QIcon::Normal, QIcon::Off);

  for (int i = 0; i < this->_urunler.count(); i++) {
    auto urun = this->_urunler[i];

    auto hucre = new QTableWidgetItem();
    hucre->setText(tr("%1").arg(urun->id()));
    hucre->setTextAlignment(Qt::AlignCenter);
    ui->tblListe->setItem(i, 0, hucre);

    hucre = new QTableWidgetItem();
    hucre->setText(tr("%1").arg(urun->urunAdi()));
    hucre->setTextAlignment(Qt::AlignCenter);
    ui->tblListe->setItem(i, 1, hucre);

    hucre = new QTableWidgetItem();
    hucre->setText(tr("%1 TL").arg(urun->fiyat()));
    hucre->setTextAlignment(Qt::AlignCenter);
    ui->tblListe->setItem(i, 2, hucre);

    auto silmeButonu = new QPushButton(this);
    silmeButonu->setText(tr("Ürün Sil"));
    silmeButonu->setIcon(silmeSimgesi);
    ui->tblListe->setCellWidget(i, 3, silmeButonu);
    connect(silmeButonu, &QPushButton::clicked, [urun, this]() {
      auto cevap = QMessageBox::question(
          nullptr, tr("Silme Onayı"),
          tr("%1 silmek istediğinize emin misiniz?").arg(urun->urunAdi()));
      if (cevap == QMessageBox::Yes) {
        Veritabani::vt().urun().sil(urun);
        this->filtrele();
      }
    });

    auto duzenlemeButonu = new QPushButton(this);
    duzenlemeButonu->setText(tr("Ürün Düzenle"));
    duzenlemeButonu->setIcon(duzenlemeSimgesi);
    ui->tblListe->setCellWidget(i, 4, duzenlemeButonu);

    connect(duzenlemeButonu, &QPushButton::clicked, [urun, this]() {
      /*MOYeniMusteriGirisFormu form(this, musteri);
      form.setStyleSheet(this->styleSheet());
      form.exec();*/
      this->filtrele();
    });
  }
}

void MOOUrunListesi::filtrele() {
  UrunVeriYoneticisi::FiltreFonksiyonu adFiltreFonksiyonu = [](Urun::Ptr) {
    return true;
  };
  UrunVeriYoneticisi::FiltreFonksiyonu fiyatFiltreFonksiyonu = [](Urun::Ptr) {
    return true;
  };

  if (!ui->leUrunAdi->text().isEmpty()) {
    auto aranan = ui->leUrunAdi->text();
    if (ui->cmbAdOperator->currentIndex() == 0) {
      // İle Başlayanlar
      adFiltreFonksiyonu = [aranan](Urun::Ptr eleman) {
        return eleman->urunAdi().toLower().startsWith(aranan.toLower());
      };
    } else if (ui->cmbAdOperator->currentIndex() == 1) {
      // İle Bitenler
      adFiltreFonksiyonu = [aranan](Urun::Ptr eleman) {
        return eleman->urunAdi().toLower().endsWith(aranan.toLower());
      };
    } else {
      // İçerenler
      adFiltreFonksiyonu = [aranan](Urun::Ptr eleman) {
        return eleman->urunAdi().toLower().contains(aranan.toLower());
      };
    }
  }

  if (ui->cmbFiyatOperator->currentIndex() == 6) {
    if (ui->spnAltSinir->value() > 0 || ui->spnUstSinir->value() > 0) {
      auto altSinir = ui->spnAltSinir->value();
      auto ustSinir = ui->spnUstSinir->value();
      fiyatFiltreFonksiyonu = [altSinir, ustSinir](Urun::Ptr urun) {
        return urun->fiyat() >= altSinir && urun->fiyat() <= ustSinir;
      };
    }
  } else {
    double deger = ui->spnAltSinir->value();
    if (deger > 0) {
      switch (ui->cmbFiyatOperator->currentIndex()) {
      case 0:
        fiyatFiltreFonksiyonu = [deger](Urun::Ptr urun) {
          return qFuzzyCompare(double(urun->fiyat()), deger) == true;
        };
        break;
      case 1:
        fiyatFiltreFonksiyonu = [deger](Urun::Ptr urun) {
          return qFuzzyCompare(double(urun->fiyat()), deger) == false;
        };
        break;
      case 2:
        fiyatFiltreFonksiyonu = [deger](Urun::Ptr urun) {
          return urun->fiyat() > deger;
        };
        break;
      case 3:
        fiyatFiltreFonksiyonu = [deger](Urun::Ptr urun) {
          return urun->fiyat() >= deger;
        };
        break;
      case 4:
        fiyatFiltreFonksiyonu = [deger](Urun::Ptr urun) {
          return urun->fiyat() < deger;
        };
        break;
      case 5:
        fiyatFiltreFonksiyonu = [deger](Urun::Ptr urun) {
          return urun->fiyat() <= deger;
        };
        break;
      }
    }
  }

  auto filtreFonksiyonu = [adFiltreFonksiyonu,
                           fiyatFiltreFonksiyonu](Urun::Ptr eleman) {
    return adFiltreFonksiyonu(eleman) && fiyatFiltreFonksiyonu(eleman);
  };

  this->_urunler = Veritabani::vt().urun().ara(filtreFonksiyonu);
  this->tabloGuncelle();
}

void MOOUrunListesi::fiyatOperatoruGuncelle() {
  if (ui->cmbFiyatOperator->currentIndex() == 6) {
    ui->lblIle->setEnabled(true);
    ui->spnUstSinir->setEnabled(true);
  } else {
    ui->lblIle->setEnabled(false);
    ui->spnUstSinir->setEnabled(false);
  }
}
