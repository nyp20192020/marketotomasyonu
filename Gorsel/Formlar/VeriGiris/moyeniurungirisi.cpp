#include "moyeniurungirisi.h"
#include "ui_moyeniurungirisi.h"

#include <QMessageBox>

#include <veritabani.h>

MOYeniUrunGirisi::MOYeniUrunGirisi(QWidget *parent) :
      QDialog(parent),
      ui(new Ui::MOYeniUrunGirisi)
{
    ui->setupUi(this);

    _urun = Veritabani::vt().urun().yeni();
    _degisiklikVarMi = false;
}

MOYeniUrunGirisi::~MOYeniUrunGirisi()
{
    delete ui;
}

void MOYeniUrunGirisi::on_btnKaydet_clicked()
{
    veriGuncelle();
    Veritabani::vt().urun().ekle(_urun);
    auto cevap = QMessageBox::question(this,
                                       tr("Ürün Kaydı Tamamlandı!"),
                                       tr("Yeni bir ürün tanımlamak ister misiniz?"),
                                       QMessageBox::Yes | QMessageBox::No,
                                       QMessageBox::Yes);
    if (cevap == QMessageBox::Yes) {
        _urun = Veritabani::vt().urun().yeni();
        gorselGuncelle();
        ui->leUrunAdi->setFocus();
        setDegisiklikVarMi(false);
    } else {
        accept();
    }
}

bool MOYeniUrunGirisi::degisiklikVarMi() const
{
    return _degisiklikVarMi;
}

void MOYeniUrunGirisi::gorselGuncelle()
{
    ui->leUrunAdi->setText(_urun->urunAdi());
    ui->spnUrunFiyati->setValue(_urun->fiyat());
}

void MOYeniUrunGirisi::veriGuncelle()
{
    _urun->setUrunAdi(ui->leUrunAdi->text());
    _urun->setFiyat(ui->spnUrunFiyati->value());
}

void MOYeniUrunGirisi::setDegisiklikVarMi(bool degisiklikVarMi)
{
    _degisiklikVarMi = degisiklikVarMi;
}

void MOYeniUrunGirisi::degisti()
{
    setDegisiklikVarMi(true);
}

void MOYeniUrunGirisi::reject()
{
    if (_degisiklikVarMi == true) {
        auto cevap = QMessageBox::question(
            this,
            tr("Bilgi Değişikliği Algılandı"),
            tr("Yaptığınız değişiklikleri kaydetmeden çıkmak istediğinize emin "
               "misiniz?"),
            QMessageBox::Yes | QMessageBox::No,
            QMessageBox::No);
        if (cevap == QMessageBox::No) {
            return;
        }
    }
    QDialog::reject();
}
