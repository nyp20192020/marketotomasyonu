#include "moyenimusterigirisformu.h"
#include "ui_moyenimusterigirisformu.h"

#include <veritabani.h>

#include <QMessageBox>

MOYeniMusteriGirisFormu::MOYeniMusteriGirisFormu(QWidget *parent,
                                                 Musteri::Ptr musteri)
    : QDialog(parent), ui(new Ui::MOYeniMusteriGirisFormu) {
  ui->setupUi(this);

  if (musteri != nullptr) {
    _eskiMusteri = musteri;
    _musteri = musteri->kopyala();
    ui->btnKaydet->setText("Güncelle");
    gorselGuncelle();
  } else {
    _musteri = Veritabani::vt().musteri().yeni();
  }
  _degisiklikVar = false;
}

MOYeniMusteriGirisFormu::~MOYeniMusteriGirisFormu() { delete ui; }

void MOYeniMusteriGirisFormu::gorselGuncelle() {
  // Müşteri nesnesindeki bilgileri ekrana aktarma
  ui->leMusteriAdi->setText(_musteri->adi());
  ui->leMusteriSoyadi->setText(_musteri->soyadi());
  ui->deMusteriDogumTarihi->setDate(_musteri->dogumTarihi());
  ui->pteMusteriAdresi->setPlainText(_musteri->adres());
}

void MOYeniMusteriGirisFormu::veriGuncelle() {
  // Ekrandaki bilgileri nesneye aktarma
  // 1. Yol görsel güncellemede yaptığımız işin aynısı!
  _musteri->setAdi(ui->leMusteriAdi->text());
  _musteri->setSoyadi(ui->leMusteriSoyadi->text());
  _musteri->setDogumTarihi(ui->deMusteriDogumTarihi->date());
  _musteri->setAdres(ui->pteMusteriAdresi->document()->toPlainText());
  // 2. Yol Sinyal Slot bağlantıları yaparak
  // connect(ui->leMusteriAdi, &QLineEdit::textChanged, _musteri.get(),
  //         &Musteri::setAdi);
  // connect(ui->leMusteriSoyadi, &QLineEdit::textChanged, _musteri.get(),
  //         &Musteri::setSoyadi);
  // connect(ui->deMusteriDogumTarihi, &QDateEdit::dateChanged, _musteri.get(),
  //         &Musteri::setDogumTarihi);
}

bool MOYeniMusteriGirisFormu::degisiklikVar() const { return _degisiklikVar; }

void MOYeniMusteriGirisFormu::setDegisiklikVar(bool degisiklikVar) {
  _degisiklikVar = degisiklikVar;
}

void MOYeniMusteriGirisFormu::gorselDegisti() { setDegisiklikVar(true); }

void MOYeniMusteriGirisFormu::reject() {
  if (_degisiklikVar) {
    auto cevap = QMessageBox::question(
        this, tr("Bilgi Değişikliği Algılandı"),
        tr("Yaptığınız değişiklikleri kaydetmeden çıkmak istediğinize emin "
           "misiniz?"),
        QMessageBox::Yes | QMessageBox::No, QMessageBox::No);
    if (cevap == QMessageBox::No) {
      return;
    }
  }
  QDialog::reject();
}

void MOYeniMusteriGirisFormu::on_btnKaydet_clicked() {
  veriGuncelle();
  if (_eskiMusteri == nullptr) {
    Veritabani::vt().musteri().ekle(_musteri);

    auto cevap = QMessageBox::question(
        this, tr("Müşteri Kaydı Tamamlandı!"),
        tr("Yeni bir müşteri tanımlamak ister misiniz?"),
        QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
    if (cevap == QMessageBox::Yes) {
      _musteri = Veritabani::vt().musteri().yeni();
      gorselGuncelle();
      ui->leMusteriAdi->setFocus();
      setDegisiklikVar(false);
    } else {
      accept();
    }

  } else {
    Veritabani::vt().musteri().duzenle(_eskiMusteri, _musteri);
    accept();
  }
}
