#ifndef MOYENISIPARISGIRISI_H
#define MOYENISIPARISGIRISI_H

#include <QDialog>

#include <VeriSiniflari/siparis.h>

namespace Ui {
class MOYeniSiparisGirisi;
}

class MOYeniSiparisGirisi : public QDialog {
  Q_OBJECT

public:
  explicit MOYeniSiparisGirisi(QWidget *parent = nullptr);
  ~MOYeniSiparisGirisi();

  bool degisiklikVar() const;
  void setDegisiklikVar(bool degisiklikVar);

private slots:
  void on_lblMusteriEkle_linkActivated(const QString &link);

  void on_lblUrunEkle_linkActivated(const QString &link);

  void on_cmbUrun_currentIndexChanged(int index);

  void on_cmbUrun_currentIndexChanged(const QString &arg1);

  void gorselDegisti();

  void on_btnKaydet_clicked();

  void reject() override;

private:
  void musteriComboBoxDoldur();
  void urunComboBoxDoldur();
  void gorselGuncelle();
  void veriGuncelle();
  void baglantilariYap();

  Ui::MOYeniSiparisGirisi *ui;
  Siparis::Ptr _siparis;
  bool _degisiklikVar;
};

#endif // MOYENISIPARISGIRISI_H
