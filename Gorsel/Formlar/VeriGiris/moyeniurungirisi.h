#ifndef MOYENIURUNGIRISI_H
#define MOYENIURUNGIRISI_H

#include <QDialog>

#include <VeriSiniflari/urun.h>

namespace Ui {
class MOYeniUrunGirisi;
}

class MOYeniUrunGirisi : public QDialog
{
    Q_OBJECT

public:
    explicit MOYeniUrunGirisi(QWidget *parent = nullptr);
    ~MOYeniUrunGirisi();

    bool degisiklikVarMi() const;

    void gorselGuncelle();
    void veriGuncelle();

private slots:
    void on_btnKaydet_clicked();
    void setDegisiklikVarMi(bool degisiklikVarMi);
    void degisti();

    void reject() override;

private:
    Ui::MOYeniUrunGirisi *ui;

    bool _degisiklikVarMi;

    Urun::Ptr _urun;
};

#endif // MOYENIURUNGIRISI_H
