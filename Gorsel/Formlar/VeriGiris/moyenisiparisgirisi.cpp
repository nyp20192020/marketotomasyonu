#include "moyenisiparisgirisi.h"
#include "ui_moyenisiparisgirisi.h"

#include <VeriSiniflari/musteri.h>
#include <VeriSiniflari/urun.h>
#include <veritabani.h>

#include <Formlar/VeriGiris/moyenimusterigirisformu.h>
#include <Formlar/VeriGiris/moyeniurungirisi.h>

#include <algorithm>

#include <QMessageBox>

MOYeniSiparisGirisi::MOYeniSiparisGirisi(QWidget *parent)
    : QDialog(parent), ui(new Ui::MOYeniSiparisGirisi) {
  ui->setupUi(this);

  this->musteriComboBoxDoldur();

  this->urunComboBoxDoldur();

  _siparis = Veritabani::vt().siparis().yeni();
  gorselGuncelle();

  baglantilariYap();
}

void MOYeniSiparisGirisi::urunComboBoxDoldur() {
  auto tumUrunler = Veritabani::vt().urun().ara([](Urun::Ptr) { return true; });
  std::sort(tumUrunler.begin(), tumUrunler.end(), [](Urun::Ptr a, Urun::Ptr b) {
    return a->urunAdi() < b->urunAdi();
  });

  ui->cmbUrun->clear();
  ui->cmbUrun->addItem(tr("-- Bir Ürün Seçin --"), 0);
  for (auto urun : tumUrunler) {
    ui->cmbUrun->addItem(urun->urunAdi(), urun->id());
  }
}

bool MOYeniSiparisGirisi::degisiklikVar() const { return _degisiklikVar; }

void MOYeniSiparisGirisi::setDegisiklikVar(bool degisiklikVar) {
  _degisiklikVar = degisiklikVar;
}

void MOYeniSiparisGirisi::gorselGuncelle() {
  if (this->_siparis->musteriId() == 0) {
    ui->cmbMusteri->setCurrentIndex(0);
  } else {
    for (int i = 1; i < ui->cmbMusteri->count(); i++) {
      Musteri::IdTuru musteriId = ui->cmbMusteri->itemData(i).toInt();
      if (this->_siparis->musteriId() == musteriId) {
        ui->cmbMusteri->setCurrentIndex(i);
        break;
      }
    }
  }

  if (this->_siparis->urunId() == 0) {
    ui->cmbUrun->setCurrentIndex(0);
  } else {
    for (int i = 1; i < ui->cmbUrun->count(); i++) {
      Musteri::IdTuru musteriId = ui->cmbUrun->itemData(i).toInt();
      if (this->_siparis->urunId() == musteriId) {
        ui->cmbUrun->setCurrentIndex(i);
        break;
      }
    }
  }

  ui->spnAdet->setValue(_siparis->adet());
  ui->spnBirimFiyat->setValue(_siparis->birimFiyat());
  ui->spnToplamFiyat->setValue(_siparis->toplamFiyat());
}

void MOYeniSiparisGirisi::veriGuncelle() {
  Musteri::IdTuru musteriId = ui->cmbMusteri->currentData().toInt();
  Urun::IdTuru urunId = ui->cmbUrun->currentData().toInt();

  if (musteriId == 0) {
    QMessageBox::critical(this, tr("Müşteri Seçilmedi"),
                          tr("Müşteri seçimi yapılmadan sipariş girilemez!"));
    return;
  }

  if (urunId == 0) {
    QMessageBox::critical(this, tr("Ürün Seçilmedi"),
                          tr("Ürün seçimi yapılmadan sipariş girilemez!"));
    return;
  }

  _siparis->setMusteriId(musteriId);
  _siparis->setUrunId(urunId);
  _siparis->setSiparisZamani(QDateTime::currentDateTime());
  _siparis->setAdet(ui->spnAdet->value());
  _siparis->setBirimFiyat(ui->spnBirimFiyat->value());
}

void MOYeniSiparisGirisi::baglantilariYap() {
    // spinbox valuechanged isimli sinyali var,
    // bu sinyalin hem QString hemde int hali var
    // bunlardan int halini istiyorsanız QOverload<int>::of yazmanız lazım
    // Sinyalin adresi lazım! &QSpinBox::valueChanged
    // KAYNAK
    connect(ui->spnAdet,
            QOverload<int>::of(&QSpinBox::valueChanged),
            // HEDEF
            // siparis nesnesi akıllı pointer olduğu için
            // connect fonksiyonu akıllı pointer değil aptal pointer istiyor!!!
            // .get() akıllı pointeri aptal pointera dönüştürüyor
            _siparis.get(),
            &Siparis::setAdet);

    connect(ui->spnBirimFiyat,
            QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            _siparis.get(),
            &Siparis::setBirimFiyat);

    connect(_siparis.get(),
            &Siparis::toplamFiyatDegisti,
            ui->spnToplamFiyat,
            &QDoubleSpinBox::setValue);

    setDegisiklikVar(false);
}

MOYeniSiparisGirisi::~MOYeniSiparisGirisi() { delete ui; }

void MOYeniSiparisGirisi::on_lblMusteriEkle_linkActivated(const QString &) {
  MOYeniMusteriGirisFormu form;
  form.exec();
  this->musteriComboBoxDoldur();
}

void MOYeniSiparisGirisi::musteriComboBoxDoldur() {

  auto tumMusteriler =
      Veritabani::vt().musteri().ara([](Musteri::Ptr) { return true; });
  std::sort(tumMusteriler.begin(), tumMusteriler.end(),
            [](Musteri::Ptr a, Musteri::Ptr b) {
              if (a->adi() == b->adi()) {
                return a->soyadi() < b->soyadi();
              }
              return a->adi() < b->adi();
            });

  ui->cmbMusteri->clear();
  ui->cmbMusteri->addItem(tr("-- Bir Müşteri Seçin --"), 0);
  for (auto musteri : tumMusteriler) {
    ui->cmbMusteri->addItem(musteri->adi() + " " + musteri->soyadi(),
                            musteri->id());
  }
}

void MOYeniSiparisGirisi::on_lblUrunEkle_linkActivated(const QString &) {
  MOYeniUrunGirisi form;
  form.exec();
  this->urunComboBoxDoldur();
}

void MOYeniSiparisGirisi::on_cmbUrun_currentIndexChanged(int) {
  Urun::IdTuru urunId = ui->cmbUrun->currentData().toInt();
  if (urunId == 0) {
    return;
  }
  auto urun = Veritabani::vt().urun().ilkiniBul(
      [urunId](Urun::Ptr urun) { return urun->id() == urunId; });

  ui->spnBirimFiyat->setValue(urun->fiyat());
}

void MOYeniSiparisGirisi::on_cmbUrun_currentIndexChanged(const QString &) {
  on_cmbUrun_currentIndexChanged(0);
}

void MOYeniSiparisGirisi::gorselDegisti() { setDegisiklikVar(true); }

void MOYeniSiparisGirisi::on_btnKaydet_clicked() {
  veriGuncelle();
  Veritabani::vt().siparis().ekle(_siparis);
  _siparis.get()->disconnect();
  auto cevap = QMessageBox::question(
      this, tr("Sipariş Kaydı Tamamlandı!"),
      tr("Yeni bir sipariş tanımlamak ister misiniz?"),
      QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
  if (cevap == QMessageBox::Yes) {
    _siparis = Veritabani::vt().siparis().yeni();
    gorselGuncelle();
    ui->cmbMusteri->setFocus();
    setDegisiklikVar(false);
  } else {
    accept();
  }
}

void MOYeniSiparisGirisi::reject() {
  if (_degisiklikVar) {
    auto cevap = QMessageBox::question(
        this, tr("Bilgi Değişikliği Algılandı"),
        tr("Yaptığınız değişiklikleri kaydetmeden çıkmak istediğinize emin "
           "misiniz?"),
        QMessageBox::Yes | QMessageBox::No, QMessageBox::No);
    if (cevap == QMessageBox::No) {
      return;
    }
  }
  QDialog::reject();
}
