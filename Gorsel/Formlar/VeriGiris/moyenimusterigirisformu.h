#ifndef MOYENIMUSTERIGIRISFORMU_H
#define MOYENIMUSTERIGIRISFORMU_H

#include <QDialog>

#include <VeriSiniflari/musteri.h>

namespace Ui {
class MOYeniMusteriGirisFormu;
}

class MOYeniMusteriGirisFormu : public QDialog {
  Q_OBJECT

public:
  explicit MOYeniMusteriGirisFormu(QWidget *parent = nullptr,
                                   Musteri::Ptr musteri = nullptr);
  ~MOYeniMusteriGirisFormu();

  bool degisiklikVar() const;
  void setDegisiklikVar(bool degisiklikVar);

public slots:
  void gorselDegisti();

  void reject() override;

private slots:
  void on_btnKaydet_clicked();

private:
  void gorselGuncelle();
  void veriGuncelle();

  Ui::MOYeniMusteriGirisFormu *ui;
  Musteri::Ptr _musteri;
  Musteri::Ptr _eskiMusteri;

  bool _degisiklikVar;
};

#endif // MOYENIMUSTERIGIRISFORMU_H
