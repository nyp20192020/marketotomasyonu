#include "momusterianalizleri.h"
#include "ui_momusterianalizleri.h"

#include <AnalizSiniflari/momusteriyegoresatislar.h>

#include <QTableWidgetItem>

MOMusteriAnalizleri::MOMusteriAnalizleri(QWidget *parent)
    : QDialog(parent), ui(new Ui::MOMusteriAnalizleri) {
  ui->setupUi(this);
  this->tabloGuncelle();
}

MOMusteriAnalizleri::~MOMusteriAnalizleri() { delete ui; }

void MOMusteriAnalizleri::tabloGuncelle() {
  MOMusteriyeGoreSatislar analiz;
  analiz.hesapla();

  auto analizSonucu = analiz.analizSonucListesi();

  QStringList basliklar;
  basliklar << tr("Müşteri No") << tr("Adı") << tr("Soyadı")
            << tr("Toplam Siparis Adedi") << tr("Sipariş Toplamı")
            << tr("Ort. Sipariş Tutarı") << tr("Sip. Std. Sapması");

  ui->tableWidget->setColumnCount(7);
  ui->tableWidget->setRowCount(analizSonucu.count());

  ui->tableWidget->setHorizontalHeaderLabels(basliklar);

  for (int i = 0; i < analizSonucu.count(); i++) {
    auto hucre = new QTableWidgetItem();
    hucre->setText(tr("%1").arg(analizSonucu[i].musteriId()));
    hucre->setTextAlignment(Qt::AlignCenter);
    ui->tableWidget->setItem(i, 0, hucre);

    hucre = new QTableWidgetItem();
    hucre->setText(tr("%1").arg(analizSonucu[i].musteriAdi()));
    hucre->setTextAlignment(Qt::AlignCenter);
    ui->tableWidget->setItem(i, 1, hucre);

    hucre = new QTableWidgetItem();
    hucre->setText(tr("%1").arg(analizSonucu[i].musteriSoyadi()));
    hucre->setTextAlignment(Qt::AlignCenter);
    ui->tableWidget->setItem(i, 2, hucre);

    hucre = new QTableWidgetItem();
    hucre->setText(tr("%1").arg(analizSonucu[i].siparisSayisi()));
    hucre->setTextAlignment(Qt::AlignCenter);
    ui->tableWidget->setItem(i, 3, hucre);

    hucre = new QTableWidgetItem();
    hucre->setText(tr("%1").arg(analizSonucu[i].siparisToplami()));
    hucre->setTextAlignment(Qt::AlignCenter);
    ui->tableWidget->setItem(i, 4, hucre);

    hucre = new QTableWidgetItem();
    hucre->setText(tr("%1").arg(analizSonucu[i].siparisOrtalamasi()));
    hucre->setTextAlignment(Qt::AlignCenter);
    ui->tableWidget->setItem(i, 5, hucre);

    hucre = new QTableWidgetItem();
    hucre->setText(tr("%1").arg(analizSonucu[i].siparisStandartSapmasi()));
    hucre->setTextAlignment(Qt::AlignCenter);
    ui->tableWidget->setItem(i, 6, hucre);
  }
}
