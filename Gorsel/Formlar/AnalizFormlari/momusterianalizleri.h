#ifndef MOMUSTERIANALIZLERI_H
#define MOMUSTERIANALIZLERI_H

#include <QDialog>

namespace Ui {
class MOMusteriAnalizleri;
}

class MOMusteriAnalizleri : public QDialog {
  Q_OBJECT

public:
  explicit MOMusteriAnalizleri(QWidget *parent = nullptr);
  ~MOMusteriAnalizleri();

  void tabloGuncelle();

private:
  Ui::MOMusteriAnalizleri *ui;
};

#endif // MOMUSTERIANALIZLERI_H
