QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Formlar/AnalizFormlari/momusterianalizleri.cpp \
    Formlar/Listeleme/momusterilistesi.cpp \
    Formlar/Listeleme/moourunlistesi.cpp \
    Formlar/VeriGiris/moyenimusterigirisformu.cpp \
    Formlar/VeriGiris/moyenisiparisgirisi.cpp \
    Formlar/VeriGiris/moyeniurungirisi.cpp \
    main.cpp \
    moanapencere.cpp

HEADERS += \
    Formlar/AnalizFormlari/momusterianalizleri.h \
    Formlar/Listeleme/momusterilistesi.h \
    Formlar/Listeleme/moourunlistesi.h \
    Formlar/VeriGiris/moyenimusterigirisformu.h \
    Formlar/VeriGiris/moyenisiparisgirisi.h \
    Formlar/VeriGiris/moyeniurungirisi.h \
    moanapencere.h

FORMS += \
    Formlar/AnalizFormlari/momusterianalizleri.ui \
    Formlar/Listeleme/momusterilistesi.ui \
    Formlar/Listeleme/moourunlistesi.ui \
    Formlar/VeriGiris/moyenimusterigirisformu.ui \
    Formlar/VeriGiris/moyenisiparisgirisi.ui \
    Formlar/VeriGiris/moyeniurungirisi.ui \
    moanapencere.ui

TRANSLATIONS += \
    Gorsel_en_US.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    Kaynaklar.qrc

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Veri/release/ -lVeri
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Veri/debug/ -lVeri
else:unix: LIBS += -L$$OUT_PWD/../Veri/ -lVeri

INCLUDEPATH += $$PWD/../Veri
DEPENDPATH += $$PWD/../Veri

# Uygulama Simgesinin eklenmesi için aşağıdaki kodun yazılması gerekiyor
RC_ICONS = Simge/Simge.ico

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Analiz/release/ -lAnaliz
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Analiz/debug/ -lAnaliz
else:unix: LIBS += -L$$OUT_PWD/../Analiz/ -lAnaliz

INCLUDEPATH += $$PWD/../Analiz
DEPENDPATH += $$PWD/../Analiz
