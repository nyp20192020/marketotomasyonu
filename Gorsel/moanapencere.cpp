#include "moanapencere.h"
#include "ui_moanapencere.h"

#include <QFileDialog>
#include <QMessageBox>

#include <QFile>
#include <QTextStream>

#include <Formlar/VeriGiris/moyenimusterigirisformu.h>
#include <Formlar/VeriGiris/moyenisiparisgirisi.h>
#include <Formlar/VeriGiris/moyeniurungirisi.h>

#include <Formlar/Listeleme/momusterilistesi.h>
#include <Formlar/Listeleme/moourunlistesi.h>

#include <Formlar/AnalizFormlari/momusterianalizleri.h>

#include <veritabani.h>

#include <QDir>
#include <QStandardPaths>

MOAnaPencere::MOAnaPencere(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MOAnaPencere) {
  ui->setupUi(this);

  QString klasorYolu =
      QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);

  QDir klasor(klasorYolu);

  if (!klasor.exists()) {
    klasor.mkpath(klasorYolu);
  }

  QString varsayilanDosyaAdi = klasorYolu + "/veri.movd";
  Veritabani::vt().yukle(varsayilanDosyaAdi);
  _qtStili = "";
  this->setStyleSheet(_qtStili);
}

MOAnaPencere::~MOAnaPencere() {

  QString klasor =
      QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);

  QString varsayilanDosyaAdi = klasor + "/veri.movd";
  Veritabani::vt().kaydet(varsayilanDosyaAdi);
  delete ui;
}

void MOAnaPencere::close() {
  auto cevap = QMessageBox::question(
      this, tr("ÇIKIŞ ONAYI"), tr("Çıkmak İstediğinize Emin misiniz?"),
      QMessageBox::Yes | QMessageBox::No, QMessageBox::No);
  if (cevap == QMessageBox::Yes) {
    QMainWindow::close();
  }
}

void MOAnaPencere::on_actionYeniMusteriEkle_triggered() {
  MOYeniMusteriGirisFormu form;
  form.setStyleSheet(this->_qtStili);
  form.exec();
}

void MOAnaPencere::on_actionYeniUrunEkle_triggered() {
  MOYeniUrunGirisi form;
  form.setStyleSheet(this->_qtStili);
  form.exec();
}

void MOAnaPencere::on_actionYeniSiparis_triggered() {
  auto musteriSayisi = Veritabani::vt().musteri().filtreyeUyanElemanSayisi(
      [](Musteri::Ptr) { return true; });
  while (musteriSayisi == 0) {
    auto cevap = QMessageBox::question(
        this, tr("Müşteri Bulunamadı!"),
        tr("Yeni müşteri tanımlamak ister misiniz?"),
        QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
    if (cevap == QMessageBox::No) {
      QMessageBox::critical(
          this, tr("Kapatılıyor"),
          tr("Müşteri tanımı olmadan sipariş girişi yapılamaz."),
          QMessageBox::Ok, QMessageBox::Ok);
      return;
    }
    on_actionYeniMusteriEkle_triggered();
    musteriSayisi = Veritabani::vt().musteri().filtreyeUyanElemanSayisi(
        [](Musteri::Ptr) { return true; });
  }

  auto urunSayisi = Veritabani::vt().urun().filtreyeUyanElemanSayisi(
      [](Urun::Ptr) { return true; });
  while (urunSayisi == 0) {
    auto cevap = QMessageBox::question(
        this, tr("Ürün Bulunamadı!"), tr("Yeni ürün tanımlamak ister misiniz?"),
        QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
    if (cevap == QMessageBox::No) {
      QMessageBox::critical(this, tr("Kapatılıyor"),
                            tr("Ürün tanımı olmadan sipariş girişi yapılamaz."),
                            QMessageBox::Ok, QMessageBox::Ok);
      return;
    }
    on_actionYeniUrunEkle_triggered();
    urunSayisi = Veritabani::vt().urun().filtreyeUyanElemanSayisi(
        [](Urun::Ptr) { return true; });
  }

  MOYeniSiparisGirisi form;
  form.setStyleSheet(this->_qtStili);
  form.exec();
}

void MOAnaPencere::on_actionKaydet_triggered() {
  QString dosyaAdi = QFileDialog::getSaveFileName(
      this, tr("Dosya Seçin"), qApp->applicationDirPath(),
      tr("Market Otomasyonu Veri Dosyası (*.movd)"));
  if (dosyaAdi != "") {
    Veritabani::vt().kaydet(dosyaAdi);
  }
}

void MOAnaPencere::on_actionAc_triggered() {
  QString dosyaAdi = QFileDialog::getOpenFileName(
      this, tr("Dosya Seçin"), qApp->applicationDirPath(),
      tr("Market Otomasyonu Veri Dosyası (*.movd)"));
  if (dosyaAdi != "") {
    Veritabani::vt().yukle(dosyaAdi);
  }
}

void MOAnaPencere::on_actionMusteriListesi_triggered() {
  MOMusteriListesi form;
  form.setStyleSheet(this->_qtStili);
  form.exec();
}

void MOAnaPencere::on_actionNormal_triggered() {
  this->_qtStili = "";
  this->setStyleSheet(this->_qtStili);
  this->temaSeciminiTemizle();
  this->ui->actionNormal->setChecked(true);
}

void MOAnaPencere::applyStyle(const QString &temaAdi) {
  QFile f(tr(":/temalar/%1.qss").arg(temaAdi));
  if (f.open(QIODevice::ReadOnly)) {
    QTextStream ts(&f);
    this->_qtStili = ts.readAll();
    this->setStyleSheet(this->_qtStili);
  }
}

void MOAnaPencere::temaDegistir() {
  QAction *act = dynamic_cast<QAction *>(sender());
  this->temaSeciminiTemizle();
  applyStyle(act->text());
  act->setChecked(true);
}

void MOAnaPencere::temaSeciminiTemizle() {
  ui->actionNormal->setChecked(false);
  ui->actionAMOLED->setChecked(false);
  ui->actionAqua->setChecked(false);
  ui->actionConsole->setChecked(false);
  ui->actionElegantDark->setChecked(false);
  ui->actionManjaro->setChecked(false);
  ui->actionMaterialDark->setChecked(false);
  ui->actionUbuntu->setChecked(false);
}

void MOAnaPencere::on_actionUrunListesi_triggered() {
  MOOUrunListesi form;
  form.setStyleSheet(this->_qtStili);
  form.exec();
}

void MOAnaPencere::on_actionM_teri_Listesi_triggered() {
  MOMusteriAnalizleri form;
  form.setStyleSheet(this->_qtStili);
  form.exec();
}
