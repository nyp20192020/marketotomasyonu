#include "momusteriyegoresatislar.h"

#include <veritabani.h>

#include <QtMath>

MOMusteriyeGoreSatislar::MOMusteriyeGoreSatislar(QObject *parent)
    : QObject(parent) {}

void MOMusteriyeGoreSatislar::hesapla() {
  _analizSonucListesi.clear();
  auto musteriler =
      Veritabani::vt().musteri().ara([](Musteri::Ptr) { return true; });

  for (auto musteri : musteriler) {
    auto siparisler =
        Veritabani::vt().siparis().ara([musteri](Siparis::Ptr siparis) {
          return siparis->musteriId() == musteri->id();
        });
    int N = 0;
    Musteri::ParaBirimi toplam = 0.0;
    Musteri::ParaBirimi mu = 0.0;
    Musteri::ParaBirimi sigma = 0.0;

    for (auto siparis : siparisler) {
      N++;
      toplam += siparis->toplamFiyat();
    }

    if (N >= 1) {
      mu = toplam / N;
    }

    if (N > 1) {
      for (auto siparis : siparisler) {
        sigma += (siparis->toplamFiyat() - mu) * (siparis->toplamFiyat() - mu);
      }

      sigma = sigma / (N - 1);
      sigma = qSqrt(sigma);
    }

    AnalizSonucu sonuc;
    sonuc.setMusteriId(musteri->id());
    sonuc.setMusteriAdi(musteri->adi());
    sonuc.setMusteriSoyadi(musteri->soyadi());
    sonuc.setSiparisSayisi(N);
    sonuc.setSiparisToplami(toplam);
    sonuc.setSiparisOrtalamasi(mu);
    sonuc.setSiparisStandartSapmasi(sigma);
    _analizSonucListesi.append(sonuc);
  }
}

const MOMusteriyeGoreSatislar::AnalizSonuclari &
MOMusteriyeGoreSatislar::analizSonucListesi() const {
  return _analizSonucListesi;
}
