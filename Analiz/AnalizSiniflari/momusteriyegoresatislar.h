#ifndef MOMUSTERIYEGORESATISLAR_H
#define MOMUSTERIYEGORESATISLAR_H

#include <QObject>
#include <QVector>

#include "AnalizVerileri/momusterianalizverisi.h"

#include "Analiz_global.h"

class ANALIZ_EXPORT MOMusteriyeGoreSatislar : public QObject {
  Q_OBJECT

public:
  typedef MOMusteriAnalizVerisi AnalizSonucu;
  typedef QVector<AnalizSonucu> AnalizSonuclari;

public:
  explicit MOMusteriyeGoreSatislar(QObject *parent = nullptr);

  void hesapla();

  const AnalizSonuclari &analizSonucListesi() const;

signals:

private:
  AnalizSonuclari _analizSonucListesi;
};

#endif // MOMUSTERIYEGORESATISLAR_H
