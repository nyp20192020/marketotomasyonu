#ifndef MOMUSTERIANALIZVERISI_H
#define MOMUSTERIANALIZVERISI_H

#include <QObject>

#include <VeriSiniflari/musteri.h>

#include "Analiz_global.h"

class ANALIZ_EXPORT MOMusteriAnalizVerisi {
public:
  explicit MOMusteriAnalizVerisi();

  Musteri::IdTuru musteriId() const;
  void setMusteriId(const Musteri::IdTuru &musteriId);

  Musteri::Metin musteriAdi() const;
  void setMusteriAdi(const Musteri::Metin &musteriAdi);

  Musteri::Metin musteriSoyadi() const;
  void setMusteriSoyadi(const Musteri::Metin &musteriSoyadi);

  Musteri::ParaBirimi siparisToplami() const;
  void setSiparisToplami(const Musteri::ParaBirimi &siparisToplami);

  Musteri::ParaBirimi siparisOrtalamasi() const;
  void setSiparisOrtalamasi(const Musteri::ParaBirimi &siparisOrtalamasi);

  Musteri::ParaBirimi siparisStandartSapmasi() const;
  void
  setSiparisStandartSapmasi(const Musteri::ParaBirimi &siparisStandartSapmasi);

  Musteri::IsaretsizTamsayi siparisSayisi() const;
  void setSiparisSayisi(const Musteri::IsaretsizTamsayi &siparisSayisi);

private:
  Musteri::IdTuru _musteriId;
  Musteri::Metin _musteriAdi;
  Musteri::Metin _musteriSoyadi;
  Musteri::IsaretsizTamsayi _siparisSayisi;
  Musteri::ParaBirimi _siparisToplami;
  Musteri::ParaBirimi _siparisOrtalamasi;
  Musteri::ParaBirimi _siparisStandartSapmasi;
};

#endif // MOMUSTERIANALIZVERISI_H
