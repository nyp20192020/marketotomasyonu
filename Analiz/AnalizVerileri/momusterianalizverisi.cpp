#include "momusterianalizverisi.h"

MOMusteriAnalizVerisi::MOMusteriAnalizVerisi() {}

Musteri::IdTuru MOMusteriAnalizVerisi::musteriId() const { return _musteriId; }

void MOMusteriAnalizVerisi::setMusteriId(const Musteri::IdTuru &musteriId) {
  _musteriId = musteriId;
}

Musteri::Metin MOMusteriAnalizVerisi::musteriAdi() const { return _musteriAdi; }

void MOMusteriAnalizVerisi::setMusteriAdi(const Musteri::Metin &musteriAdi) {
  _musteriAdi = musteriAdi;
}

Musteri::Metin MOMusteriAnalizVerisi::musteriSoyadi() const {
  return _musteriSoyadi;
}

void MOMusteriAnalizVerisi::setMusteriSoyadi(
    const Musteri::Metin &musteriSoyadi) {
  _musteriSoyadi = musteriSoyadi;
}

Musteri::ParaBirimi MOMusteriAnalizVerisi::siparisToplami() const {
  return _siparisToplami;
}

void MOMusteriAnalizVerisi::setSiparisToplami(
    const Musteri::ParaBirimi &siparisToplami) {
  _siparisToplami = siparisToplami;
}

Musteri::ParaBirimi MOMusteriAnalizVerisi::siparisOrtalamasi() const {
  return _siparisOrtalamasi;
}

void MOMusteriAnalizVerisi::setSiparisOrtalamasi(
    const Musteri::ParaBirimi &siparisOrtalamasi) {
  _siparisOrtalamasi = siparisOrtalamasi;
}

Musteri::ParaBirimi MOMusteriAnalizVerisi::siparisStandartSapmasi() const {
  return _siparisStandartSapmasi;
}

void MOMusteriAnalizVerisi::setSiparisStandartSapmasi(
    const Musteri::ParaBirimi &siparisStandartSapmasi) {
  _siparisStandartSapmasi = siparisStandartSapmasi;
}

Musteri::IsaretsizTamsayi MOMusteriAnalizVerisi::siparisSayisi() const {
  return _siparisSayisi;
}

void MOMusteriAnalizVerisi::setSiparisSayisi(
    const Musteri::IsaretsizTamsayi &siparisSayisi) {
  _siparisSayisi = siparisSayisi;
}
