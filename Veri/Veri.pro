QT -= gui

TEMPLATE = lib
DEFINES += VERI_LIBRARY

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    VeriSiniflari/musteri.cpp \
    VeriSiniflari/siparis.cpp \
    VeriSiniflari/urun.cpp \
    VeriSiniflari/temelverisinifi.cpp \
    VeriYonetim/musteriveriyoneticisi.cpp \
    VeriYonetim/siparisveriyoneticisi.cpp \
    VeriYonetim/urunveriyoneticisi.cpp \
    veri.cpp \
    veritabani.cpp

HEADERS += \
    VeriSiniflari/musteri.h \
    VeriSiniflari/siparis.h \
    VeriSiniflari/urun.h \
    VeriSiniflari/temelverisinifi.h \
    VeriYonetim/musteriveriyoneticisi.h \
    VeriYonetim/siparisveriyoneticisi.h \
    VeriYonetim/temelveriyonetimsinifi.h \
    VeriYonetim/urunveriyoneticisi.h \
    Veri_global.h \
    veri.h \
    veritabani.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target
