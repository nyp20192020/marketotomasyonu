#include "musteri.h"

Musteri::Musteri(QObject *parent) : TemelVeriSinifi(parent)
{
    _adi = "";
    _adres = "";
    _soyadi = "";
    _dogumTarihi = Tarih::currentDate();
}

Musteri::Metin Musteri::adi() const
{
    return _adi;
}

void Musteri::setAdi(const Metin &adi)
{
    if (_adi != adi) {
        _adi = adi;
        adiDegisti(_adi);
    }
}

Musteri::Metin Musteri::soyadi() const
{
    return _soyadi;
}

void Musteri::setSoyadi(const Metin &soyadi)
{
    if (_soyadi != soyadi) {
        _soyadi = soyadi;
        soyadiDegisti(_soyadi);
    }
}

Musteri::Tarih Musteri::dogumTarihi() const
{
    return _dogumTarihi;
}

void Musteri::setDogumTarihi(const Tarih &dogumTarihi)
{
    if (_dogumTarihi != dogumTarihi) {
        _dogumTarihi = dogumTarihi;
        dogumTarihiDegisti(_dogumTarihi);
    }
}

Musteri::Metin Musteri::adres() const
{
    return _adres;
}

void Musteri::setAdres(const Metin &adres)
{
    if (_adres != adres) {
        _adres = adres;
        adresDegisti(_adres);
    }
}
