#ifndef MUSTERI_H
#define MUSTERI_H

#include "temelverisinifi.h"

#include <memory>

#include <Veri_global.h>

/**
 * @brief Müşteri Sınıfı
 *
 * Market Otomasyonu yazılımında kullanılacak temel müşteri sınıfı.
 */
class VERI_EXPORT Musteri : public TemelVeriSinifi {
  Q_OBJECT

public:
  /**
   * @brief Veri tanımı
   *
   * Sınıfın kendi içerisinde kullanılmak için tanımlanmış veri tanımı.
   */
  typedef Musteri Veri;

  /**
   * @brief Ptr tanımı
   *
   * Sınıftan pointer oluşturmak için kullanılan veri tanımı.
   */
  typedef std::shared_ptr<Veri> Ptr;

public:
  /**
   * @brief Musteri Sınıfı Varsayılan İlklendirici
   *
   * Bu ilklendirici, müşteri sınıfındaki değerlere 0 veya benzeri değerler
   * atayacaktır.
   *
   * @param parent Ata Sınıf (Qt'nin sınıf hiyerarşisi için gerekli)
   */
  explicit Musteri(QObject *parent = nullptr);

  /**
   * @brief adi Müşteri Adı Okuyucusu
   *
   * Müşteri adı değişkeni için okuyucu fonksiyon.
   *
   * @return Müşteri Adı
   */
  Metin adi() const;

  /**
   * @brief setAdi Müşteri Adı Atayıcısı
   *
   * Müşteri adı değişkeni için atayıcı fonksiyon.
   *
   * @param adi Yeni ad değeri
   */
  void setAdi(const Metin &adi);

  /**
   * @brief soyadi Soyadı Okuyucu Fonksiyon
   *
   * Soyadı değişkeni için okuyucu fonksiyon.
   *
   * @return Soyadı değişkeninin değeri
   */
  Metin soyadi() const;
  void setSoyadi(const Metin &soyadi);

  Tarih dogumTarihi() const;
  void setDogumTarihi(const Tarih &dogumTarihi);

  Metin adres() const;
  void setAdres(const Metin &adres);

  static Ptr yeni() { return std::make_shared<Veri>(); }

  /**
   * @brief kopyala Nesne Kopyası oluşturan fonksiyon
   *
   * Bu fonksiyon, mevcut bir nesnenin kopyasını oluşturmak amacı ile
   * kullanılır. Kopyalama işlemi sonucunda tipik bir pointer kopyalaması değil,
   * nesnenin farklı bir bellek bölgesinde yeni hali oluşur.
   *
   * @return Oluşturulan Kopya Nesne.
   */
  Ptr kopyala() {
    Ptr sonuc = Musteri::yeni();

    sonuc->_adi = this->_adi;
    sonuc->_adres = this->_adres;
    sonuc->_soyadi = this->_soyadi;
    sonuc->_dogumTarihi = this->_dogumTarihi;
    sonuc->_id = this->_id;

    return sonuc;
  }

signals:
  void adiDegisti(const Metin &adi);
  void soyadiDegisti(const Metin &soyadi);
  void dogumTarihiDegisti(const Tarih &dogumTarihi);
  void adresDegisti(const Metin &adres);

private:
  Metin _adi;
  Metin _soyadi;
  Tarih _dogumTarihi;
  Metin _adres;
};

#endif // MUSTERI_H
