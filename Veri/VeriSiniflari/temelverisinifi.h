#ifndef TEMELVERISINIFI_H
#define TEMELVERISINIFI_H

#include <QObject>

#include <QDate>
#include <QDateTime>
#include <QString>

#include "Veri_global.h"

class VERI_EXPORT TemelVeriSinifi : public QObject {
  Q_OBJECT
public:
  typedef QString Metin;
  typedef unsigned long long IdTuru;
  typedef float ParaBirimi;
  typedef QDate Tarih;
  typedef QDateTime TarihSaat;
  typedef unsigned int IsaretsizTamsayi;
  typedef int Tamsayi;

public:
  explicit TemelVeriSinifi(QObject *parent = nullptr);

  IdTuru id() const;
  void setId(const IdTuru &id);

signals:
  void idDegisti(const IdTuru &id);

protected:
  IdTuru _id;
};

#endif // TEMELVERISINIFI_H
