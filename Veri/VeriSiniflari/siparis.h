#ifndef SIPARIS_H
#define SIPARIS_H

#include "temelverisinifi.h"
#include <memory>

#include <Veri_global.h>

class VERI_EXPORT Siparis : public TemelVeriSinifi {
  Q_OBJECT

public:
  typedef Siparis Veri;
  typedef std::shared_ptr<Veri> Ptr;

public:
  explicit Siparis(QObject *parent = nullptr);

  IdTuru musteriId() const;
  void setMusteriId(const IdTuru &musteriId);

  IdTuru urunId() const;
  void setUrunId(const IdTuru &urunId);

  TarihSaat siparisZamani() const;
  void setSiparisZamani(const TarihSaat &siparisZamani);

  IsaretsizTamsayi adet() const;
  void setAdet(const IsaretsizTamsayi &adet);

  ParaBirimi birimFiyat() const;
  void setBirimFiyat(const ParaBirimi &birimFiyat);

  inline ParaBirimi toplamFiyat() { return _adet * _birimFiyat; }

  static Ptr yeni() { return std::make_shared<Veri>(); }

  Ptr kopyala() {
    auto kopya = yeni();
    kopya->_musteriId = this->_musteriId;
    kopya->_urunId = this->_urunId;
    kopya->_siparisZamani = this->_siparisZamani;
    kopya->_adet = this->_adet;
    kopya->_birimFiyat = this->_birimFiyat;
    kopya->_id = this->_id;
    return kopya;
  }

signals:
  void musteriIdDegisti(const IdTuru &musteriId);
  void urunIdDegisti(const IdTuru &urunId);
  void siparisZamaniDegisti(const TarihSaat &siparisZamani);
  void adetDegisti(const IsaretsizTamsayi &adet);
  void birimFiyatDegisti(const ParaBirimi &birimFiyat);
  void toplamFiyatDegisti(const ParaBirimi &toplamFiyat);

private:
  IdTuru _musteriId;
  IdTuru _urunId;
  TarihSaat _siparisZamani;
  IsaretsizTamsayi _adet;
  ParaBirimi _birimFiyat;
};

#endif // SIPARIS_H
