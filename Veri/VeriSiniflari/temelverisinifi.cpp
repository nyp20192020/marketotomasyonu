#include "temelverisinifi.h"

TemelVeriSinifi::TemelVeriSinifi(QObject *parent) : QObject(parent)
{
    _id = 0;
}

TemelVeriSinifi::IdTuru TemelVeriSinifi::id() const
{
    return _id;
}

void TemelVeriSinifi::setId(const TemelVeriSinifi::IdTuru &id)
{
    if (_id != id) {
        _id = id;
        idDegisti(_id);
    }
}
