#include "urun.h"

#include <QtMath>

Urun::Urun(QObject *parent) : TemelVeriSinifi(parent) {
  _urunAdi = "";
  _fiyat = 0.0;
}

Urun::Metin Urun::urunAdi() const { return _urunAdi; }

void Urun::setUrunAdi(const Metin &urunAdi) {
  if (_urunAdi != urunAdi) {
    _urunAdi = urunAdi;
    urunAdiDegisti(_urunAdi);
  }
}

Urun::ParaBirimi Urun::fiyat() const { return _fiyat; }

void Urun::setFiyat(const ParaBirimi &fiyat) {
  if (qFuzzyCompare(_fiyat, fiyat) == false) {
    _fiyat = fiyat;
    fiyatDegisti(_fiyat);
  }
}
