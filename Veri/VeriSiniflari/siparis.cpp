#include "siparis.h"

Siparis::Siparis(QObject *parent) : TemelVeriSinifi(parent) {
  this->_adet = 0;
  this->_musteriId = 0;
  this->_urunId = 0;
  this->_birimFiyat = 0.0f;
  this->_siparisZamani = QDateTime::currentDateTime();
}

Siparis::IdTuru Siparis::musteriId() const { return _musteriId; }

void Siparis::setMusteriId(const IdTuru &musteriId) {
  if (_musteriId != musteriId) {
    _musteriId = musteriId;
    musteriIdDegisti(_musteriId);
  }
}

Siparis::IdTuru Siparis::urunId() const { return _urunId; }

void Siparis::setUrunId(const IdTuru &urunId) {
  if (_urunId != urunId) {
    _urunId = urunId;
    urunIdDegisti(_urunId);
  }
}

Siparis::TarihSaat Siparis::siparisZamani() const { return _siparisZamani; }

void Siparis::setSiparisZamani(const TarihSaat &siparisZamani) {
  if (_siparisZamani != siparisZamani) {
    _siparisZamani = siparisZamani;
    siparisZamaniDegisti(_siparisZamani);
  }
}

Siparis::IsaretsizTamsayi Siparis::adet() const { return _adet; }

void Siparis::setAdet(const IsaretsizTamsayi &adet) {
  if (_adet != adet) {
    _adet = adet;
    adetDegisti(_adet);
    toplamFiyatDegisti(toplamFiyat());
  }
}

Siparis::ParaBirimi Siparis::birimFiyat() const { return _birimFiyat; }

void Siparis::setBirimFiyat(const ParaBirimi &birimFiyat) {
  if (qFuzzyCompare(_birimFiyat, birimFiyat) == false) {
    _birimFiyat = birimFiyat;
    birimFiyatDegisti(_birimFiyat);
    toplamFiyatDegisti(toplamFiyat());
  }
}
