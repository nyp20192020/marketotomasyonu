#ifndef URUN_H
#define URUN_H

#include <QObject>

#include <memory>

#include "temelverisinifi.h"

#include <Veri_global.h>

class VERI_EXPORT Urun : public TemelVeriSinifi {
  Q_OBJECT
public:
  typedef Urun Veri;
  typedef std::shared_ptr<Veri> Ptr;

public:
  explicit Urun(QObject *parent = nullptr);

  Metin urunAdi() const;
  void setUrunAdi(const Metin &urunAdi);

  ParaBirimi fiyat() const;
  void setFiyat(const ParaBirimi &fiyat);

  static Ptr yeni() { return std::make_shared<Veri>(); }

  Ptr kopyala() {
    auto kopya = yeni();
    kopya->_urunAdi = this->_urunAdi;
    kopya->_fiyat = this->_fiyat;
    kopya->_id = this->_id;
    return kopya;
  }

signals:
  void urunAdiDegisti(const Metin &urunAdi);
  void fiyatDegisti(const ParaBirimi &fiyat);

private:
  Metin _urunAdi;
  ParaBirimi _fiyat;
};

#endif // URUN_H
