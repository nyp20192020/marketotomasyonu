#ifndef SIPARISVERIYONETIMSINIFI_H
#define SIPARISVERIYONETIMSINIFI_H

#include <QObject>

#include "temelveriyonetimsinifi.h"
#include <VeriSiniflari/siparis.h>

#include "Veri_global.h"

class VERI_EXPORT SiparisVeriYonetimSinifi : public QObject, TemelVeriYonetimSinifi<Siparis>
{
    Q_OBJECT
public:
    explicit SiparisVeriYonetimSinifi(QObject *parent = nullptr);

signals:
    void elemanEklendi(Pointer ptr);
    void elemanSilindi(Pointer ptr);
    void elemanDegisti(Pointer eski, Pointer yeni);
};

#endif // SIPARISVERIYONETIMSINIFI_H
