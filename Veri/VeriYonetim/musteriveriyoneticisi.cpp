#include "musteriveriyoneticisi.h"

MusteriVeriYoneticisi::MusteriVeriYoneticisi(QObject *parent) : QObject(parent)
{

}

QDataStream &operator<<(QDataStream &dosya, const Musteri::Ptr &musteri)
{
    dosya << musteri->id() << musteri->adi() << musteri->soyadi() <<
             musteri->dogumTarihi() << musteri->adres();

    return dosya;
}

QDataStream &operator>>(QDataStream &dosya, Musteri::Ptr &musteri)
{
    Musteri::IdTuru id;
    Musteri::Metin adi, soyadi, adres;
    Musteri::Tarih dogumTarihi;

    dosya >> id >> adi >> soyadi >> dogumTarihi >> adres;

    auto yeniMusteri = Musteri::yeni();

    yeniMusteri->setId(id);
    yeniMusteri->setAdi(adi);
    yeniMusteri->setSoyadi(soyadi);
    yeniMusteri->setDogumTarihi(dogumTarihi);
    yeniMusteri->setAdres(adres);

    musteri = yeniMusteri;

    return dosya;
}
