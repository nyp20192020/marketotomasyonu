#include "siparisveriyoneticisi.h"

SiparisVeriYoneticisi::SiparisVeriYoneticisi(QObject *parent) : QObject(parent)
{

}

QDataStream & operator<<(QDataStream &dosya, const Siparis::Ptr &siparis) {

    dosya << siparis->id() << siparis->musteriId() << siparis->urunId()
          << siparis->birimFiyat() << siparis->adet() << siparis->siparisZamani();

    return dosya;
}

QDataStream & operator>>(QDataStream &dosya, Siparis::Ptr &siparis) {
    Siparis::IdTuru id, musteriId, urunId;
    Siparis::IsaretsizTamsayi adet;
    Siparis::ParaBirimi birimFiyat;
    Siparis::TarihSaat zaman;

    dosya >> id >> musteriId >> urunId >> birimFiyat >> adet >> zaman;

    auto yeniSiparis = Siparis::yeni();

    yeniSiparis->setId(id);
    yeniSiparis->setMusteriId(musteriId);
    yeniSiparis->setUrunId(urunId);
    yeniSiparis->setAdet(adet);
    yeniSiparis->setBirimFiyat(birimFiyat);
    yeniSiparis->setSiparisZamani(zaman);

    siparis = yeniSiparis;

    return dosya;
}
