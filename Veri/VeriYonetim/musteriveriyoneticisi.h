#ifndef MUSTERIVERIYONETICISI_H
#define MUSTERIVERIYONETICISI_H

#include <QObject>
#include <QDataStream>

#include "temelveriyonetimsinifi.h"
#include <VeriSiniflari/musteri.h>

#include <Veri_global.h>

class VERI_EXPORT MusteriVeriYoneticisi
    : public QObject,
      public TemelVeriYonetimSinifi<Musteri> {
  Q_OBJECT
public:
  explicit MusteriVeriYoneticisi(QObject *parent = nullptr);

signals:
  void elemanEklendi(Pointer ptr);
  void elemanSilindi(Pointer ptr);
  void elemanDegisti(Pointer eski, Pointer yeni);
};

QDataStream & operator<<(QDataStream &dosya, const Musteri::Ptr &musteri);
QDataStream & operator>>(QDataStream &dosya, Musteri::Ptr &musteri);

#endif // MUSTERIVERIYONETICISI_H
