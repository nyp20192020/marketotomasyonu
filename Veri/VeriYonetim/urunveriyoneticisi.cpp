#include "urunveriyoneticisi.h"

UrunVeriYoneticisi::UrunVeriYoneticisi(QObject *parent) : QObject(parent) {}

QDataStream & operator<<(QDataStream &dosya, const Urun::Ptr &urun) {
    dosya << urun->id() << urun->urunAdi() << urun->fiyat();

    return dosya;
}

QDataStream & operator>>(QDataStream &dosya, Urun::Ptr &urun) {
    Urun::IdTuru id;
    Urun::Metin adi;
    Urun::ParaBirimi fiyat;

    dosya >> id >> adi >> fiyat;

    auto yeniUrun = Urun::yeni();

    yeniUrun->setId(id);
    yeniUrun->setUrunAdi(adi);
    yeniUrun->setFiyat(fiyat);

    urun = yeniUrun;

    return dosya;
}
