#ifndef SIPARISVERIYONETICISI_H
#define SIPARISVERIYONETICISI_H

#include <QObject>

#include "temelveriyonetimsinifi.h"

#include <VeriSiniflari/siparis.h>

#include <Veri_global.h>

class VERI_EXPORT SiparisVeriYoneticisi
    : public QObject,
      public TemelVeriYonetimSinifi<Siparis> {
  Q_OBJECT
public:
  explicit SiparisVeriYoneticisi(QObject *parent = nullptr);

signals:
  void elemanEklendi(Pointer ptr);
  void elemanSilindi(Pointer ptr);
  void elemanDegisti(Pointer eski, Pointer yeni);
};

QDataStream & operator<<(QDataStream &dosya, const Siparis::Ptr &siparis);
QDataStream & operator>>(QDataStream &dosya, Siparis::Ptr &siparis);

#endif // SIPARISVERIYONETICISI_H
