#ifndef URUNVERIYONETIMSINIFI_H
#define URUNVERIYONETIMSINIFI_H

#include <QObject>

#include "temelveriyonetimsinifi.h"
#include <VeriSiniflari/urun.h>

#include "Veri_global.h"

class VERI_EXPORT UrunVeriYonetimSinifi : public QObject, public TemelVeriYonetimSinifi<Urun>
{
    Q_OBJECT
public:
    explicit UrunVeriYonetimSinifi(QObject *parent = nullptr);

signals:
    void elemanEklendi(Pointer ptr);
    void elemanSilindi(Pointer ptr);
    void elemanDegisti(Pointer eski, Pointer yeni);
};

#endif // URUNVERIYONETIMSINIFI_H
