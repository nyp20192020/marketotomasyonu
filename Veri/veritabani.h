#ifndef VERITABANI_H
#define VERITABANI_H

#include <QObject>

#include <VeriYonetim/musteriveriyoneticisi.h>
#include <VeriYonetim/siparisveriyoneticisi.h>
#include <VeriYonetim/urunveriyoneticisi.h>

#include <Veri_global.h>

class VERI_EXPORT Veritabani : public QObject {
  Q_OBJECT
private:
  explicit Veritabani(QObject *parent = nullptr);

public:
  static Veritabani &vt();

  MusteriVeriYoneticisi &musteri();

  SiparisVeriYoneticisi &siparis();

  UrunVeriYoneticisi &urun();

  void kaydet(QString dosyaAdi);

  void yukle(QString dosyaAdi);

private:
  MusteriVeriYoneticisi _musteri;
  SiparisVeriYoneticisi _siparis;
  UrunVeriYoneticisi _urun;

signals:
  void urunEklendi(Urun::Ptr urun);
  void urunSilindi(Urun::Ptr urun);
};

#endif // VERITABANI_H
