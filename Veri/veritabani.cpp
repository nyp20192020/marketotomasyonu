#include "veritabani.h"
#include <QFile>
#include <QDataStream>

Veritabani::Veritabani(QObject *parent) : QObject(parent) {
  connect(&_urun, &UrunVeriYoneticisi::elemanEklendi, this,
          &Veritabani::urunEklendi);
}

Veritabani &Veritabani::vt() {
  static Veritabani nesne;
  return nesne;
}

MusteriVeriYoneticisi &Veritabani::musteri() { return _musteri; }

SiparisVeriYoneticisi &Veritabani::siparis() { return _siparis; }

UrunVeriYoneticisi &Veritabani::urun() { return _urun; }

void Veritabani::kaydet(QString dosyaAdi)
{
    QFile dosya(dosyaAdi);
    if(dosya.open(QIODevice::WriteOnly)) {
        QDataStream ds(&dosya);
        _musteri.kaydet(ds);
        _urun.kaydet(ds);
        _siparis.kaydet(ds);
        dosya.close();
    }
}

void Veritabani::yukle(QString dosyaAdi)
{
    QFile dosya(dosyaAdi);
    if(dosya.open(QIODevice::ReadOnly)) {
        QDataStream ds(&dosya);
        _musteri.yukle(ds);
        _urun.yukle(ds);
        _siparis.yukle(ds);
        dosya.close();
    }
}
